module gitlab.com/review4

go 1.20

require (
	github.com/go-chi/chi v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.2.0
)
