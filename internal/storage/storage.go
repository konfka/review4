package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/review4/internal/model"
)

type StorageInterface interface {
	GetUsers() ([]model.User, error)
	UpdateUser(user model.User, id int) (model.User, error)
	DeleteUser(id int) error
	CreateUser(user model.User) (model.User, error)
}

type Storage struct {
	db *sqlx.DB
}

func NewStorage(db *sqlx.DB) *Storage {
	return &Storage{db: db}
}

func (s *Storage) GetUsers() ([]model.User, error) {
	var res []model.User
	query := `SELECT * FROM users`

	rows, err := s.db.Query(query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var user model.User
		err = rows.Scan(&user.ID, &user.FirstName, &user.LastName, &user.Age, &user.Email, &user.Phone, &user.Address, &user.City, &user.Country)
		if err != nil {
			return nil, err
		}
		res = append(res, user)
	}
	return res, nil
}

func (s *Storage) UpdateUser(user model.User, id int) (model.User, error) {
	var updatedUser model.User
	query := `UPDATE users SET first_name = $1, last_name = $2, age = $3, email = $4, phone = $5, address = $6, city = $7, country = $8 WHERE id=$9
			  RETURNING id, first_name, last_name, age, email, phone, address, city, country`
	err := s.db.QueryRow(query, user.FirstName, user.LastName, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country, id).Scan(&updatedUser.ID, &updatedUser.FirstName, &updatedUser.LastName, &updatedUser.Age, &updatedUser.Email, &updatedUser.Phone, &updatedUser.Address, &updatedUser.City, &updatedUser.Country)
	if err != nil {
		return model.User{}, err
	}
	return updatedUser, nil
}

func (s *Storage) DeleteUser(id int) error {
	query := `DELETE FROM users WHERE id=$1`
	_, err := s.db.Exec(query, id)
	if err != nil {
		return err
	}
	return nil
}

func (s *Storage) CreateUser(user model.User) (model.User, error) {
	var id int
	err := s.db.QueryRow("INSERT INTO users (first_name, last_name, age, email, phone, address, city, country) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id",
		user.FirstName, user.LastName, user.Age, user.Email, user.Phone, user.Address, user.City, user.Country).Scan(&id)
	if err != nil {
		return model.User{}, err
	}
	user.ID = id

	return user, nil
}
