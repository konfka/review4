package database

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	"log"
)

type PostgresDB struct {
	Db *sqlx.DB
}

func CreateDBAndTable() (*PostgresDB, error) {
	err := godotenv.Load()
	if err != nil {
		log.Fatalln("Error loading .env file")
	}

	var config map[string]string

	config, err = godotenv.Read()
	if err != nil {
		return nil, err
	}

	connStr := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config["DB_HOST"], config["DB_PORT"], config["DB_USER"], config["DB_PASSWORD"], config["DB_NAME"])

	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	fmt.Println("Connected to the database")

	create := `CREATE TABLE IF NOT EXISTS Users ( 
    	id INT PRIMARY KEY, 
    	first_name VARCHAR(50), 
    	last_name VARCHAR(50), 
		age INT, email VARCHAR(100), 
		phone VARCHAR(20), 
		address VARCHAR(100), 
		city VARCHAR(50), 
		country VARCHAR(50));`

	_, err = db.Exec(create)
	if err != nil {
		return nil, err
	}
	return &PostgresDB{Db: db}, nil
}

func (d *PostgresDB) AddUsers() error {
	_, err := d.Db.Exec(`INSERT INTO Users 
    (id, first_name, last_name, age, email, phone, address, city, country) 
	VALUES (1, 'John', 'Doe', 25, 'johndoe@example.com', '123-456-7890', '123 Main St', 'New York', 'USA'), 
	       (2, 'Jane', 'Smith', 32, 'janesmith@example.com', '987-654-3210', '456 Elm St', 'Los Angeles', 'USA'), 
	       (3, 'Mike', 'Johnson', 41, 'mikejohnson@example.com', '555-123-4567', '789 Oak Ave', 'Chicago', 'USA'), 
	       (4, 'Emily', 'Brown', 29, 'emilybrown@example.com', '111-222-3333', '321 Pine Rd', 'London', 'UK'), 
	       (5, 'Alex', 'Lee', 37, 'alexlee@example.com', '444-555-6666', '567 Maple Dr', 'Toronto', 'Canada');`)
	if err != nil {
		return err
	}
	return nil
}
