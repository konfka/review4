package controller

import "github.com/go-chi/chi"

func Router(c *Controller) *chi.Mux {
	r := chi.NewRouter()
	r.Get("/get", c.GetUsers)
	r.Post("/update/{id}", c.UpdateUser)
	r.Delete("/delete/{id}", c.DeleteUser)
	r.Post("/create", c.CreateUser)

	return r
}
