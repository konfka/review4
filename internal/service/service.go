package service

import (
	"gitlab.com/review4/internal/model"
	"gitlab.com/review4/internal/storage"
)

type ServiceInterfase interface {
	GetUsers() ([]model.User, error)
	UpdateUser(user model.User, id int) (model.User, error)
	DeleteUser(id int) error
	CreateUser(user model.User) (model.User, error)
}

type Service struct {
	repo storage.StorageInterface
}

func NewService(repository storage.StorageInterface) *Service {
	return &Service{
		repo: repository,
	}
}

func (s *Service) GetUsers() ([]model.User, error) {
	return s.repo.GetUsers()
}

func (s *Service) UpdateUser(user model.User, id int) (model.User, error) {
	return s.repo.UpdateUser(user, id)
}

func (s *Service) DeleteUser(id int) error {
	return s.repo.DeleteUser(id)
}

func (s *Service) CreateUser(user model.User) (model.User, error) {
	return s.repo.CreateUser(user)
}
