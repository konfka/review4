package app

import (
	"gitlab.com/review4/internal/controller"
	"gitlab.com/review4/internal/database"
	"gitlab.com/review4/internal/service"
	"gitlab.com/review4/internal/storage"
)

func Run(Db *database.PostgresDB) {
	repo := storage.NewStorage(Db.Db)
	serv := service.NewService(repo)
	contr := controller.NewController(serv)

	contr.Start()
}
