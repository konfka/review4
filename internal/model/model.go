package model

type User struct {
	ID        int    `database:"id" json:"id"`
	FirstName string `database:"first_name" json:"first_name"`
	LastName  string `database:"last_name" json:"last_name"`
	Age       int    `database:"age" json:"age"`
	Email     string `database:"email" json:"email"`
	Phone     string `database:"phone" json:"phone"`
	Address   string `database:"address" json:"address"`
	City      string `database:"city" json:"city"`
	Country   string `database:"country" json:"country"`
}
