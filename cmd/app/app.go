package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/review4/internal/app"
	"gitlab.com/review4/internal/database"
	"log"
)

func main() {

	db, err := database.CreateDBAndTable()
	if err != nil {
		log.Println(err)
	}

	//err = database.AddUsers()
	if err != nil {
		log.Println(err)
	}

	app.Run(db)
}
